package com.alphatec.builderreport.core;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Clase para la configuracion de la aplicacion
 *
 * Created by Jorge Henao on 13/02/2018.
 */

public class BuilderReportAplication extends Application{

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate() {
        super.onCreate();

        //Inicializacion de sthetho
        Stetho.initializeWithDefaults(this);
    }
}
