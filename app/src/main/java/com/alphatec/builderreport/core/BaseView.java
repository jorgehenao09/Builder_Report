package com.alphatec.builderreport.core;

/**
 * Created by Jorge Henao on 23/01/2018.
 */

public interface BaseView {

    /**
     * Metodo utilizado para limpiar los datos del formulario
     */
    void initializeForm();

    /**
     * Metodo encargado de limpiar los mensajes de error mostrados en pantalla
     */
    void cleanErrors();

    /**
     * Muestra un mensaje de espera en la pantalla mientras se realiza el procesamiento
     */
    void showWaitMessage();

    /**
     * Oculta el mensaje de espera
     */
    void hideWaitMessage();

    /**
     * Muestra un mensaje que indica que hay errores por corregir
     */
    void showAlertMessage();

    /**
     * Muestra una ventana con un mensaje de error
     *
     * @param title
     * @param message
     */
    void showDialogMessage(String title, String message);

    /**
     * Muestra un mensaje para notificar el resultado de una operacion
     *
     * @param textoMensaje
     */
    void showMessageInformation(String textoMensaje);

    /**
     * Muestra el contenido del formulario
     */
    void showForm();

    /**
     * Oculta el contenido del formulario
     */
    void hideForm();
}
