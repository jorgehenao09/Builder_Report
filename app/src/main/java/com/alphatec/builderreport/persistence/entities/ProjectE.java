package com.alphatec.builderreport.persistence.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.alphatec.builderreport.persistence.converter.DateConverter;

import java.util.Date;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

@Entity(tableName = "PROJECT")
@TypeConverters(DateConverter.class)
public class ProjectE {

    @PrimaryKey(autoGenerate = true)
    private Long id;
    @ColumnInfo(name = "name_project")
    private String nameProject;
    private String dempartment;
    private String city;
    private String address;
    private Date date;
    private String email;
    private String routePhoto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameProject() {
        return nameProject;
    }

    public void setNameProject(String nameProject) {
        this.nameProject = nameProject;
    }

    public String getDempartment() {
        return dempartment;
    }

    public void setDempartment(String dempartment) {
        this.dempartment = dempartment;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoutePhoto() {
        return routePhoto;
    }

    public void setRoutePhoto(String routePhoto) {
        this.routePhoto = routePhoto;
    }
}
