package com.alphatec.builderreport.persistence.converter;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

public class DateConverter {

    @TypeConverter
    public Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
