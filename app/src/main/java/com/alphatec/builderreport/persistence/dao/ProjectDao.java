package com.alphatec.builderreport.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.alphatec.builderreport.persistence.entities.ProjectE;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

@Dao
public interface ProjectDao {

    /**
     * Retorna el listado de proyectos almacenados
     *
     * @return
     */
    @Query("SELECT * FROM PROJECT")
    Flowable<List<ProjectE>> getAllProjects();

    @Query("SELECT * FROM PROJECT WHERE id=:id")
    ProjectE getProject(int id);

    /**
     * Inserta un nuevo proyecto en la BD
     *
     * @param projectE
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ProjectE projectE);

    /**
     * Actualiza un nuevo proyecto en la BD
     *
     * @param projectE
     */
    @Update
    void update(ProjectE projectE);

    /**
     * Elimina un nuevo proyecto en la BD
     *
     * @param projectE
     */
    @Delete
    void delete(ProjectE projectE);
}
