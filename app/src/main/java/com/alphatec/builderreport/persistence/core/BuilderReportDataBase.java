package com.alphatec.builderreport.persistence.core;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.alphatec.builderreport.persistence.dao.ProjectDao;
import com.alphatec.builderreport.persistence.entities.ProjectE;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

@Database(entities = { ProjectE.class }, version = 1)
public abstract class BuilderReportDataBase extends RoomDatabase {

    private static final String DB_NAME = "BuilderReportDB.db";
    private static volatile BuilderReportDataBase instance;

    public static synchronized BuilderReportDataBase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    /**
     * Metodo encargado de crear la instancia de la Base de datos
     *
     * @param context
     * @return
     */
    private static BuilderReportDataBase create(final Context context) {
        return Room.databaseBuilder(
                context,
                BuilderReportDataBase.class,
                DB_NAME).build();
    }

    public abstract ProjectDao getProjectDao();
}
