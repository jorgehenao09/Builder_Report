package com.alphatec.builderreport.util;

import com.alphatec.builderreport.util.EnumResponseType;

/**
 * Created by Jorge Henao on 23/01/2018.
 */

public class ReplyMessage {

    /**
     * Tipo de la respuesta del mensaje
     */
    private EnumResponseType responseType;

    /**
     * Codigo o nombre del componente que genera el mensaje
     */
    private String originMessage;

    /**
     * Codigo del mensaje de respuesta
     */
    private String codeMessage;

    /**
     * Parametros que acompañan el mensaje. Son opcionales
     */
    private Object[] parameterMessage;

    /**
     * Construye la respuesta de un proceso indicando su tipo, origen, codigo del mensaje y los parametros
     *
     * @param responseType Tipo de la respuesta: EXITOSO, ADVERTENCIA, INFORMACION
     * @param originMessage Componente o campo origen del mensaje
     * @param codeMessage Codigo del mensaje
     * @param parameterMessage Parametros para el mensaje (Opcional)
     */
    public ReplyMessage(EnumResponseType responseType, String originMessage, String codeMessage, Object... parameterMessage) {
        this.responseType = responseType;
        this.originMessage = originMessage;
        this.codeMessage = codeMessage;
        this.parameterMessage = parameterMessage;
    }

    /**
     * Construye la respuesta de un proceso indicando su tipo, codigo del mensaje y los parametros
     *
     * @param responseType Tipo de la respuesta: EXITOSO, ADVERTENCIA, INFORMACION
     * @param codeMessage Codigo del mensaje
     * @param parameterMessage Parametros para el mensaje (Opcional)
     */
    public ReplyMessage(EnumResponseType responseType, String codeMessage, Object... parameterMessage) {
        this.responseType = responseType;
        this.codeMessage = codeMessage;
        this.parameterMessage = parameterMessage;
    }

    public EnumResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(EnumResponseType responseType) {
        this.responseType = responseType;
    }

    public String getOriginMessage() {
        return originMessage;
    }

    public void setOriginMessage(String originMessage) {
        this.originMessage = originMessage;
    }

    public String getCodeMessage() {
        return codeMessage;
    }

    public void setCodeMessage(String codeMessage) {
        this.codeMessage = codeMessage;
    }

    public Object[] getParameterMessage() {
        return parameterMessage;
    }

    public void setParameterMessage(Object[] parameterMessage) {
        this.parameterMessage = parameterMessage;
    }
}
