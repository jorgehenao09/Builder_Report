package com.alphatec.builderreport.util;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jorge Henao on 10/12/2017.
 */

public class SelectorDateListener implements View.OnClickListener {

    private TextView mTextViewDate;
    private Context mContext;
    private String mDate;

    public SelectorDateListener(Context context, TextView textViewDate, String date){
        this.mContext = context;
        this.mTextViewDate = textViewDate;
        this.mDate = date;
    }

    @Override
    public void onClick(View view) {

        // Obtener fecha actual
        Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR);
        int currentMonth = c.get(Calendar.MONTH);
        int currentDay = c.get(Calendar.DAY_OF_MONTH);

        if(!Util.itsEmptyData(mDate)){
            Date startDate = Util.formatDate(mDate);
            //Obtener fecha registrada
            c.setTime(startDate);
            currentYear = c.get(Calendar.YEAR);
            currentMonth = c.get(Calendar.MONTH);
            currentDay = c.get(Calendar.DAY_OF_MONTH);
        }

        // Retornar en nueva instancia del dialogo selector de fecha
        DatePickerDialog dl = new DatePickerDialog(this.mContext,
                new DatePickerDialog.OnDateSetListener(){

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        SelectorDateListener.this.mTextViewDate.setText(year + "/" + (monthOfYear+1) + "/" + dayOfMonth);
                    }
                },
                currentYear,
                currentMonth,
                currentDay);

        dl.show();
    }
}
