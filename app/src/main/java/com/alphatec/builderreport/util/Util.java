package com.alphatec.builderreport.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jorge Henao on 10/12/2017.
 */
public class Util {

    private static Logger logger = Logger.getLogger(Util.class);

    public static final String FORMAT_DATE = "yyyy/MM/dd";

    /**
     * Verifica si una cadena esta vacia
     *
     * @param string
     * @return
     */
    public static boolean itsEmptyData(String string) {
        if (string == null || string.length() == 0) {
            return true;
        }

        String cadenaAux = string.trim();

        if (cadenaAux.length() == 0) {
            return true;
        }

        return false;
    }

    /**
     * Returns the value of a date in a text string, using the supplied format. If the date is null
     * returns an empty string if an exception occurs during conversion, it returns null
     *
     * @param date
     * @return formatted date
     */
    public static String formatDate(Date date) {
        return formatDate(date, FORMAT_DATE);
    }

    /**
     * Returns the value of a date in a text string, using the supplied format. If the date is null
     * returns an empty string if an exception occurs during conversion, it returns null
     *
     * @param fecha
     * @return formatted date
     */
    private static String formatDate(Date fecha, String formatoFecha) {
        if (fecha == null) {
            return null;
        }

        String s = null;

        try {
            if (fecha != null) {
                SimpleDateFormat sdf = new SimpleDateFormat(formatoFecha);
                sdf.setLenient(false);
                s = sdf.format(fecha);
            }

            return s;
        } catch (Exception t) {
            logger.error("Error al formatear la fecha " + fecha + " al formato:" + FORMAT_DATE, t);
            return null;
        }
    }

    /**
     * Elimina las horas, minutos y segundos de una fecha dada.
     *
     * @param fecha
     *                Fecha
     * @return Date
     */
    public static Date deleteHourDate(Date fecha) {

        Calendar cl = Calendar.getInstance();
        cl.setTime(fecha);

        cl.set(Calendar.HOUR_OF_DAY, 0);
        cl.set(Calendar.MINUTE, 0);
        cl.set(Calendar.SECOND, 0);
        cl.set(Calendar.MILLISECOND, 0);

        return cl.getTime();
    }

    /**
     * Returns the value of a date in a date object, using the supplied format. If the text string is null
     * returns an null object
     *
     * @param date
     * @return formatted date
     */
    public static Date formatDate(String date) {
        if (itsEmptyData(date)) {
            return null;
        }

        try {
            if (date != null) {
                SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
                sdf.setLenient(false);
                return sdf.parse(date);
            }
        } catch (Exception t) {
            logger.error("Error al formatear la fecha " + date + " al formato:" + FORMAT_DATE, t);
        }

        return null;
    }

    /**
     * Metodo utilizado para validar un email
     *
     * @param email
     * @return
     */
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Actualizar la imagen con respecto a la que selecciona el usuario
     *
     * @param data
     */
    public static String updateSelectImage(Intent data, ContentResolver contentResolver, ImageView imageView) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = contentResolver.query(selectedImage, filePathColumn, null, null, null);

        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);

        Bitmap bitmap = BitmapEfficient.decodeSampledBitmapFromResource(picturePath, 80, 60);

        bitmap = fixImage(picturePath, bitmap);

        imageView.setImageBitmap(bitmap);

        cursor.close();

        return picturePath;
    }

    /**
     * Actualizar la imagen con respecto a la que toma del usuario
     *
     * @param context
     * @param contentResolver
     * @param imageView
     * @param imageUri
     * @return
     */
    public static String updateImage(Context context, ContentResolver contentResolver, ImageView imageView, Uri imageUri) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        // Calculate inSampleSize
        float scale = context.getResources().getDisplayMetrics().density;
        options.inSampleSize = (int) (10 * scale);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        //Cursor que apunta a la ruta de la imagen
        Cursor cursor = contentResolver.query(imageUri, new String[]{MediaStore.Images.Media.DATA}, null, null, null);

        int column_index_data = cursor != null ? cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA) : 0;
        cursor.moveToFirst();

        //Ruta de la imagen
        String picturePath = cursor.getString(column_index_data);

        //Tamaño de la imagen
        int width = 300;
        int height = 300;

        //Se redimensiona la imagen
        Bitmap bitmap = BitmapEfficient.decodeSampledBitmapFromFile(imageUri,width, height, context);

        //Se posiciona de forma adecuada la imagen
        bitmap = fixImage(picturePath, bitmap);

        imageView.getWidth();
        imageView.setImageBitmap(bitmap);
        cursor.close();

        return picturePath;
    }

    /**
     * Organiza de forma adecuada la imagen, independiente de si el usuario tomo la foto en landscape o normal
     *
     * @param picturePath
     * @param imageBitmap
     * @return
     */
    private static Bitmap fixImage(String picturePath, Bitmap imageBitmap) {

        try {
            ExifInterface exif = new ExifInterface(picturePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            Matrix matrix = new Matrix();

            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.postRotate(270);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.postRotate(90);

            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.postRotate(180);
            } else if (orientation == ExifInterface.ORIENTATION_NORMAL) {

            }

            imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.getWidth(), imageBitmap.getHeight(), matrix, true);

        } catch (IOException e) {
            Log.e("ExifInterface", e.getLocalizedMessage());
        }

        return imageBitmap;
    }

    /**
     * Metodo utilizado para comprimir y organizar la fotografia
     *
     * @param pathImage
     */
    public static void resizeImage(String pathImage){
        try
        {
            int inWidth;
            int inHeight;

            int dstWidth = 600;
            int dstHeight = 600;

            InputStream in = new FileInputStream(pathImage);

            // decode image size (decode metadata only, not the whole image)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);
            in.close();

            // save width and height
            inWidth = options.outWidth;
            inHeight = options.outHeight;

            // decode full image pre-resized
            in = new FileInputStream(pathImage);
            options = new BitmapFactory.Options();
            // calc rought re-size (this is no exact resize)
            options.inSampleSize = Math.max(inWidth/dstWidth, inHeight/dstHeight);
            // decode full image
            Bitmap roughBitmap = BitmapFactory.decodeStream(in, null, options);

            // calc exact destination size
            Matrix m = new Matrix();
            RectF inRect = new RectF(0, 0, roughBitmap.getWidth(), roughBitmap.getHeight());
            RectF outRect = new RectF(0, 0, dstWidth, dstHeight);
            m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);
            float[] values = new float[9];
            m.getValues(values);

            // resize bitmap
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(roughBitmap, (int) (roughBitmap.getWidth() * values[0]), (int) (roughBitmap.getHeight() * values[4]), true);

            // save image
            try
            {
                FileOutputStream out = new FileOutputStream(pathImage);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            }
            catch (Exception e)
            {
                Log.e("Image", e.getMessage(), e);
            }
        }
        catch (IOException e)
        {
            Log.e("Image", e.getMessage(), e);
        }
    }
}
