package com.alphatec.builderreport.util;

/**
 * Created by Jorge Henao on 23/01/2018.
 */

public enum EnumResponseType {

    INFORMATION,
    WARNING,
    ERROR
}
