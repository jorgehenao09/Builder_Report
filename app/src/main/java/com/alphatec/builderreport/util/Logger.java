package com.alphatec.builderreport.util;

import android.util.Log;

/**
 * Clase para hacer el log de la aplicacion
 *
 * Created by Jorge Henao on 10/12/2017.
 */
public class Logger {

    private Class<?> origin;

    private String br = "BUILDER_REPORT->";

    /**
     * Constructor privado que recibe la clase origen del log
     *
     * @param origin
     */
    private Logger(Class<?> origin){
        this.origin = origin;
    }

    /**
     * Construye un nuevo objeto para escritura de logs de la clase enviada
     *
     * @param origin
     * @return
     */
    public static Logger getLogger(Class<?> origin){
        return new Logger(origin);
    }

    /**
     * Escribe en el log un mensaje con prioridad DEBUG
     *
     * @param message Mensaje
     */
    public void debug(String message){
        Log.d(br, origin.getName() + ": " + message);
    }

    /**
     * Escribe en el log un mensaje con prioridad INFO
     *
     * @param message Mensaje
     */
    public void info(String message){
        Log.i(br, origin.getName() + ": " + message);
    }

    /**
     * Escribe en el log un mensaje con prioridad ERROR
     *
     * @param message Mensaje
     */
    public void error(String message){
        Log.e(br, origin.getName() + ": " + message);
    }

    /**
     * Escribe en el log un mensaje con prioridad DEBUG
     *
     * @param message Mensaje
     * @param tr Excepcion
     */
    public void error(String message, Throwable tr){
        Log.e(br, origin.getName() + ": " + message, tr);
    }
}
