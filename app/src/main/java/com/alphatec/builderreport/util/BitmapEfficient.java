package com.alphatec.builderreport.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

public class BitmapEfficient {

    public static Bitmap decodeSampledBitmapFromFile(Uri imageUri, int reqWidth, int reqHeight, Context context) {

        ParcelFileDescriptor parcelFD;
        try {
            int inWidth;
            int inHeight;

            parcelFD = context.getContentResolver().openFileDescriptor(imageUri, "r");
            FileDescriptor imageSource = parcelFD != null ? parcelFD.getFileDescriptor() : null;

            // decode image size (decode metadata only, not the whole image)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeFileDescriptor(imageSource, null, options);

            // save width and height
            inWidth = options.outWidth;
            inHeight = options.outHeight;

            // decode full image pre-resized
            options = new BitmapFactory.Options();
            // calc rought re-size (this is no exact resize)
            options.inSampleSize = Math.max(inWidth / reqWidth, inHeight / reqHeight);
            Log.i("decode file " , ""+options.inSampleSize) ;
            // decode full image

            Bitmap roughBitmap =BitmapFactory.decodeFileDescriptor(imageSource, null, options);

            // calc exact destination size
            Matrix m = new Matrix();
            RectF inRect = new RectF(0, 0, roughBitmap.getWidth(), roughBitmap.getHeight());
            RectF outRect = new RectF(0, 0, reqWidth, reqHeight);
            m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);
            float[] values = new float[9];
            m.getValues(values);

            // resize bitmap
            return Bitmap.createScaledBitmap(roughBitmap, (int) (roughBitmap.getWidth() * values[0]), (int) (roughBitmap.getHeight() * values[4]), true);
        } catch (IOException e) {

            try {
                parcelFD = context.getContentResolver().openFileDescriptor(imageUri, "r");

                FileDescriptor imageSource = parcelFD != null ? parcelFD.getFileDescriptor() : null;

                // First decode with inJustDecodeBounds=true to check dimensions
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;

                BitmapFactory.decodeFileDescriptor(imageSource, null, options);

                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight, 1);
                Log.e("error decode file "+options.inSampleSize ,  e.getLocalizedMessage()) ;
                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                return BitmapFactory.decodeFileDescriptor(imageSource, null, options);

            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
                return null;
            }
        }
    }


    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight, int size) {
        // Raw height and width of image
        final int height = options.outHeight;

        final int width = options.outWidth;

        int inSampleSize = size;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
                calculateInSampleSize(options, reqWidth, reqHeight, inSampleSize);
            }
//            calculateInSampleSize(options, reqWidth, reqHeight, inSampleSize);
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String path, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight,1);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }
}
