package com.alphatec.builderreport.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alphatec.builderreport.R;
import com.alphatec.builderreport.activities.myProjects.interfaces.MyProjectsView;
import com.alphatec.builderreport.persistence.entities.ProjectE;
import com.alphatec.builderreport.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jorge Henao on 13/02/2018.
 */

public class RecyclerProjectAdapter extends RecyclerView.Adapter<RecyclerProjectAdapter.ProjectViewHolder>{

    private List<ProjectE> mProjectList;
    private Context mContext;
    private MyProjectsView mView;

    public RecyclerProjectAdapter(List<ProjectE> projectList, MyProjectsView view, Context context) {
        this.mProjectList = projectList;
        this.mContext = context;
        this.mView = view;
    }

    /**
     * Este metodo asocia el layout item_recycler_projects.xml con el recyclerView (infla el layout)
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_projects, parent, false);

        return new ProjectViewHolder(view);
    }

    /**
     * Este metodo modifica los elementos de la clase ProjectViewHolder con respecto a cada objeto de la lista items
     *
     * @param projectViewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(ProjectViewHolder projectViewHolder, int position) {
        ProjectE project = mProjectList.get(position);

        //Se hace la correlacion de datos con los campos de la tarjeta
        projectViewHolder.mName.setText(project.getNameProject());
        //projectViewHolder.mNumberReports.setText(mView.getNumberReports(project.getId()));
        projectViewHolder.mNumberReports.setText("0");
        projectViewHolder.mCreationDate.setText(Util.formatDate(project.getDate()));
        projectViewHolder.mLocation.setText(project.getDempartment() + " - " + project.getCity() + " - " + project.getAddress());

        //Acciones realizadas al momento de dar clic sobre compartir
        projectViewHolder.mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Compartir", Toast.LENGTH_SHORT).show();
            }
        });

        //Acciones realizadas al momento de dar clic sobre editar
        projectViewHolder.mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Editar", Toast.LENGTH_SHORT).show();
            }
        });

        //Acciones realizadas al momento de dar clic sobre eliminar
        projectViewHolder.mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Eliminar", Toast.LENGTH_SHORT).show();
            }
        });

        //Acciones realizadas al momento de dar clic sobre agregar reporte
        projectViewHolder.mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Agregar reporte", Toast.LENGTH_SHORT).show();
            }
        });

        //Acciones realizadas al momento de dar clic sobre ver lista de reportes
        projectViewHolder.mListReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Ver lista", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProjectList.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_project_share) ImageView mShare;
        @BindView(R.id.item_project_edit) ImageView mEdit;
        @BindView(R.id.item_project_delete) ImageView mDelete;

        @BindView(R.id.item_project_name) TextView mName;
        @BindView(R.id.item_project_number_reports) TextView mNumberReports;
        @BindView(R.id.item_project_creation_date) TextView mCreationDate;
        @BindView(R.id.item_project_location) TextView mLocation;

        @BindView(R.id.item_project_add) Button mAdd;
        @BindView(R.id.item_project_list) Button mListReports;

        public ProjectViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
