package com.alphatec.builderreport.constants;

/**
 * Created by Jorge Henao on 23/01/2018.
 */

public interface Constants {

    //Campos de formulario
    String PHOTO = "PHOTO";
    String NAME = "NAME";
    String DEPARTMENT = "DEPARTMENT";
    String CITY = "CITY";
    String ADDRESS = "ADDRESS";
    String DATE = "DATE";
    String EMAIL = "EMAIL";

    //Mensajes de error
    String REQUIRED_VALUE = "requiredValue";
    String INVALID_VALUE = "invalidValue";
    String NUMNER_OUT_RANGE = "numberOutRange";
    String COODINATE_OUT_RANGE = "coordinateOutRange";
    String SIZE_OUT_RANGE = "sizeOutRange";
    String INAVLID_EMAIL = "invalidEmail";
    String DATE_GREATER_THAN_CURRENT = "dateGreaterThanCurrent";
    String PHOTO_DOES_NOT_EXIST = "photoDoesNotExist";
}
