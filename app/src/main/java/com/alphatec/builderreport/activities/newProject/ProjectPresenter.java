package com.alphatec.builderreport.activities.newProject;

import com.alphatec.builderreport.activities.newProject.interfaces.ProjectModel;
import com.alphatec.builderreport.activities.newProject.interfaces.ProjectView;
import com.alphatec.builderreport.util.ReplyMessage;
import com.alphatec.builderreport.util.ResponseProcess;
import com.alphatec.builderreport.constants.Constants;
import com.alphatec.builderreport.persistence.entities.ProjectE;
import com.alphatec.builderreport.util.EnumResponseType;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

public class ProjectPresenter {

    private ProjectView mView;
    private ProjectModel mModel;

    public ProjectPresenter(ProjectView view, ProjectModel model) {
        this.mView = view;
        this.mModel = model;
    }

    /**
     * Verifica si debe inicializar el formulario con datos
     */
    public void initialize() {
        //Se oculta el formulario mientras se realiza la carga de los datos iniciales
        mView.hideForm();

        if(mView.getProject() != null){
            mView.initializeFormWithData(mView.getProject());
        }

        mView.showForm();
    }

    /**
     * Ordena al interactor realizar el proceso de validacion, el metodo procesa la respuesta del proceso
     *
     * @param project
     */
    public void saveProject(ProjectE project) {
        //Se muestra un mensaje de espera mientras el proceso se realiza
        mView.showWaitMessage();

        //Se limpia el formulario de posibles errores anteriores
        mView.cleanErrors();

        //Se oculta el formulario mientras se realiza el procesamiento
        mView.hideForm();

        ResponseProcess response = mModel.saveProject(project);

        //Si la respuesta contiene mensajes de error se procesan
        if(response.containsErrorMessages()){
            //Se recorren cada uno de los mensajes de la respuesta
            for(ReplyMessage mr : response.getMessages()) {
                //Se procesan solo los mensajes de error
                if (EnumResponseType.ERROR.equals(mr.getResponseType())) {

                    if (Constants.PHOTO.equals(mr.getOriginMessage())){
                        mView.showErrorMessagePhoto(mr.getCodeMessage(), mr.getParameterMessage());
                    }

                    if(Constants.NAME.equals(mr.getOriginMessage())){
                        mView.showErrorMessageName(mr.getCodeMessage(), mr.getParameterMessage());
                    }

                    if(Constants.DEPARTMENT.equals(mr.getOriginMessage())){
                        mView.showErrorMessageDepartment(mr.getCodeMessage(), mr.getParameterMessage());
                    }

                    if(Constants.CITY.equals(mr.getOriginMessage())){
                        mView.showErrorMessageCity(mr.getCodeMessage(), mr.getParameterMessage());
                    }

                    if(Constants.ADDRESS.equals(mr.getOriginMessage())){
                        mView.showErrorMessageAddress(mr.getCodeMessage(), mr.getParameterMessage());
                    }

                    if(Constants.DATE.equals(mr.getOriginMessage())){
                        mView.showErrorMessageDate(mr.getCodeMessage(), mr.getParameterMessage());
                    }

                    if(Constants.EMAIL.equals(mr.getOriginMessage())){
                        mView.showErrorMessageEmail(mr.getCodeMessage(), mr.getParameterMessage());
                    }
                }
            }

            mView.hideWaitMessage();
            mView.showAlertMessage();
            mView.showForm();
        }
        //Si la respuesta es exitosa se muestra el mensaje de exito y se prepara el formulario
        else {

            mView.hideWaitMessage();
            mView.initializeForm();
            mView.showForm();
            mView.showSuccessfulSavingMessage();
            mView.returnResultActivity();
        }

    }

    /**
     * Libera las instacias del view y del model
     */
    public void onDestroy() {
        this.mView = null;
        this.mModel = null;
    }
}
