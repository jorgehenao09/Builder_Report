package com.alphatec.builderreport.activities.newReport;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.alphatec.builderreport.R;
import com.alphatec.builderreport.activities.newReport.interfaces.ReportModel;
import com.alphatec.builderreport.activities.newReport.interfaces.ReportView;
import com.alphatec.builderreport.core.BaseActivity;
import com.alphatec.builderreport.util.SelectorDateListener;

import butterknife.BindView;

public class ReportActivity extends BaseActivity implements ReportView {

    @BindView(R.id.form_report_date) TextView mDate;

    @BindView(R.id.form_photo) ImageView mImage;

    @BindView(R.id.form_til_title_report) TextInputLayout mTilTitle;
    @BindView(R.id.form_til_place_report) TextInputLayout mTilPlace;
    @BindView(R.id.form_til_area_report) TextInputLayout mTilArea;
    @BindView(R.id.form_til_description_report) TextInputLayout mTilDescription;

    @BindView(R.id.form_tie_title_report) TextInputEditText mTieTitle;
    @BindView(R.id.form_tie_place_report) TextInputEditText mTiePlace;
    @BindView(R.id.form_tie_area_report) TextInputEditText mTieArea;
    @BindView(R.id.form_tie_description_report) TextInputEditText mTieDescription;

    @BindView(R.id.form_type_work) Spinner mTypeWork;
    @BindView(R.id.form_unity) Spinner mUnity;

    @BindView(R.id.form_report_date_error) TextView mErrorDate;
    @BindView(R.id.form_type_work_error) TextView mErrorTypeWork;
    @BindView(R.id.form_image_error) TextView mErrorImage;

    private ReportPresenter mPresenter;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildPresenter() {
        super.buildPresenter();

        ReportModel model = new ReportModelImpl();

        mPresenter = new ReportPresenter(this, model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {
        super.initializeView();

        mDate.setOnClickListener(new SelectorDateListener(this, mDate, null));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getLayoutId() {
        return R.layout.activity_report;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getParentContainerId() {
        return R.id.activity_form;
    }


    @Override
    public void initializeForm() {

    }

    @Override
    public void cleanErrors() {

    }
}
