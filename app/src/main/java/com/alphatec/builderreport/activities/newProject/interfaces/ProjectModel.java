package com.alphatec.builderreport.activities.newProject.interfaces;

import com.alphatec.builderreport.util.ResponseProcess;
import com.alphatec.builderreport.persistence.entities.ProjectE;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

public interface ProjectModel {

    /**
     * Define los paso al guardar un proyecto
     *
     * @param project
     * @return
     */
    ResponseProcess saveProject(ProjectE project);
}
