package com.alphatec.builderreport.activities.newProject;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alphatec.builderreport.R;
import com.alphatec.builderreport.activities.newProject.interfaces.ProjectModel;
import com.alphatec.builderreport.activities.newProject.interfaces.ProjectView;
import com.alphatec.builderreport.core.BaseActivity;
import com.alphatec.builderreport.persistence.core.BuilderReportDataBase;
import com.alphatec.builderreport.persistence.entities.ProjectE;
import com.alphatec.builderreport.util.SelectorDateListener;
import com.alphatec.builderreport.util.Util;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

public class ProjectActivity extends BaseActivity implements ProjectView{

    @BindView(R.id.project_photo) ImageView mImagePhoto;

    @BindView(R.id.project_tie_name) TextInputEditText mTieName;
    @BindView(R.id.project_tie_department) TextInputEditText mTieDepartment;
    @BindView(R.id.project_tie_city) TextInputEditText mTieCity;
    @BindView(R.id.project_tie_address) TextInputEditText mTieAddress;
    @BindView(R.id.project_tie_email) TextInputEditText mTieEmail;

    @BindView(R.id.project_til_name) TextInputLayout mTilName;
    @BindView(R.id.project_til_department) TextInputLayout mTilDepartment;
    @BindView(R.id.project_til_city) TextInputLayout mTilCity;
    @BindView(R.id.project_til_address) TextInputLayout mTilAddress;
    @BindView(R.id.project_til_email) TextInputLayout mTilEmail;

    @BindView(R.id.project_date) TextView mValueDate;

    @BindView(R.id.project_image_error) TextView mErrorImage;
    @BindView(R.id.project_date_error) TextView mErrorDate;

    public static final String PARAMETER_PROJECT = "PARAMETER_PROJECT";

    private static final int ACTION_TAKE_PHOTO = 0;
    private static final int ACTION_SELECT_PHOTO = 1;

    private Uri mImageUri;
    private String mRoutePhoto;

    private ProjectPresenter mPresenter;
    private ProjectE mProject;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildPresenter() {
        super.buildPresenter();

        ProjectModel model = new ProjectModelImpl(BuilderReportDataBase.getInstance(this));

        mPresenter = new ProjectPresenter(this, model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {
        super.initializeView();

        if(permisosRequeridosOtorgados) {

            Bundle parametros = getIntent().getExtras();

            //Se verifica si el intent contiene extras y se procede a llenar el formulario
            if (parametros != null) {
                mProject = (ProjectE) parametros.get(PARAMETER_PROJECT);
            }

            mPresenter.initialize();

            //Se agrega el listener para el text view de la fecha de proyecto
            mValueDate.setOnClickListener(new SelectorDateListener(this, mValueDate, mValueDate.getText().toString()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getLayoutId() {
        return R.layout.activity_project;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getParentContainerId() {
        return R.id.activity_project;
    }

    /**
     * Retorna la lista de permisos que necesita la pantalla
     */
    @Override
    protected List<String> getPermissionList() {
        return Arrays.asList(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeToolbar() {
        super.initializeToolbar();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form, menu);

        return true;
    }


    /**
     * Acciones realizadas al momento de seleccionar las opciones del actionBar
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            //Acción guardar
            case R.id.action_save:
                saveProject();
                return true;

            //Acción atrás
            case android.R.id.home:
                endActivity(RESULT_CANCELED);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Obtiene los datos del formulario y ordena al presenter guardar el proyecto
     */
    private void saveProject() {
        //Se construye el DTO de project
        if (mProject == null){
            mProject = new ProjectE();
        }

        //Se verifica que los campos no se encuentren vacios
        if(!Util.itsEmptyData(mTieName.getText().toString())) {
            mProject.setNameProject(mTieName.getText().toString());
        }

        if(!Util.itsEmptyData(mRoutePhoto)) {
            mProject.setRoutePhoto(mRoutePhoto);
        }

        if(!Util.itsEmptyData(mTieDepartment.getText().toString())) {
            mProject.setDempartment(mTieDepartment.getText().toString());
        }

        if(!Util.itsEmptyData(mTieCity.getText().toString())) {
            mProject.setCity(mTieCity.getText().toString());
        }

        if(!Util.itsEmptyData(mTieAddress.getText().toString())) {
            mProject.setAddress(mTieAddress.getText().toString());
        }

        if(!Util.itsEmptyData(mValueDate.getText().toString())) {
            mProject.setDate(Util.formatDate(mValueDate.getText().toString()));
        }

        if(!Util.itsEmptyData(mTieEmail.getText().toString())) {
            mProject.setEmail(mTieEmail.getText().toString());
        }

        mPresenter.saveProject(mProject);
    }

    /**
     * Acciones realizadas al momento de dar clic sobre el boton "Tomar foto"
     *
     * @param view
     */
    public void btnTakePhoto(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String fileName = Environment.getExternalStorageDirectory() + "/BuilderReport " + String.valueOf(System.currentTimeMillis()) + ".jpg";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);

        mImageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, ACTION_TAKE_PHOTO);
    }

    /**
     * Acciones realizadas al momento de dar clic en el boton 'Seleccionar de la galeria'
     *
     * @param view
     */
    public void btnTakeGallery(View view){

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // Start the Intent
        startActivityForResult(galleryIntent, ACTION_SELECT_PHOTO);
    }

    /**
     * Retorna el POJO de proyecto
     *
     * @return
     */
    @Override
    public ProjectE getProject() {
        return mProject;
    }

    /**
     * Metodo utilizado para llenar el formulario
     *
     * @param project
     */
    @Override
    public void initializeFormWithData(ProjectE project) {
        //Se verifica que la ruta de la fotografia no sea una cadena vacia
        if(!Util.itsEmptyData(project.getRoutePhoto())){
            File FilePhoto = new File(project.getRoutePhoto());

            //Se verifica que exista el archivo de la fotografia
            if(FilePhoto.exists() && FilePhoto.length() > 0){
                //Se convierte la fotografia de String a Bitmap
                mImagePhoto.setImageBitmap(BitmapFactory.decodeFile(project.getRoutePhoto()));
            }
        }

        //Se hace la correlacion de datos
        mTieName.setText(project.getNameProject());
        mTieDepartment.setText(project.getDempartment());
        mTieCity.setText(project.getCity());
        mTieAddress.setText(project.getAddress());
        mValueDate.setText(Util.formatDate(project.getDate()));
        mTieEmail.setText(project.getEmail());
    }

    /**
     * Regresa a la actividad donde fue invocada con codigo OK
     */
    @Override
    public void returnResultActivity() {
        endActivity(RESULT_OK);
    }

    /**
     * Realiza las acciones necesarias para notificar el exito en la operacion de guardar un proyecto
     */
    @Override
    public void showSuccessfulSavingMessage() {
        showMessageInformation(getString(R.string.information_saved_project));
    }

    /**
     * Metodo utilizado para mostrar errores asociados con la fotografia
     *
     * @param messageCode
     * @param messageParameter
     */
    @Override
    public void showErrorMessagePhoto(String messageCode, Object... messageParameter) {
        mErrorImage.setVisibility(View.VISIBLE);
    }

    /**
     * Metodo utilizado para mostrar errores asociados con el nombre
     *
     * @param messageCode
     * @param messageParameter
     */
    @Override
    public void showErrorMessageName(String messageCode, Object... messageParameter) {
        mTilName.setError(getFormattedMessage(messageCode, messageParameter));
    }

    /**
     * Metodo utilizado para mostrar errores asociados con el departamento
     *
     * @param messageCode
     * @param messageParameter
     */
    @Override
    public void showErrorMessageDepartment(String messageCode, Object... messageParameter) {
        mTilDepartment.setError(getFormattedMessage(messageCode, messageParameter));
    }

    /**
     * Metodo utilizado para mostrar errores asociados con la ciudad
     *
     * @param messageCode
     * @param messageParameter
     */
    @Override
    public void showErrorMessageCity(String messageCode, Object... messageParameter) {
        mTilCity.setError(getFormattedMessage(messageCode, messageParameter));
    }

    /**
     * Metodo utilizado para mostrar errores asociados con la dirección
     *
     * @param messageCode
     * @param messageParameter
     */
    @Override
    public void showErrorMessageAddress(String messageCode, Object... messageParameter) {
        mTilAddress.setError(getFormattedMessage(messageCode, messageParameter));
    }

    /**
     * Metodo utilizado para mostrar errores asociados con la fecha
     *
     * @param messageCode
     * @param messageParameter
     */
    @Override
    public void showErrorMessageDate(String messageCode, Object... messageParameter) {
        mErrorDate.setText(getFormattedMessage(messageCode, messageParameter));
        mErrorDate.setVisibility(View.VISIBLE);
    }

    /**
     * Metodo utilizado para mostrar errores asociados con el correo electrónico
     *
     * @param messageCode
     * @param messageParameter
     */
    @Override
    public void showErrorMessageEmail(String messageCode, Object... messageParameter) {
        mTilEmail.setError(getFormattedMessage(messageCode, messageParameter));
    }

    /**
     * Metodo utilizado para limpiar los datos del formulario
     */
    @Override
    public void initializeForm() {
        mTieName.setText("");
        mTieDepartment.setText("");
        mTieCity.setText("");
        mTieAddress.setText("");
        mValueDate.setText("");
        mTieEmail.setText("");
    }

    /**
     * Metodo encargado de limpiar los mensajes de error mostrados en pantalla
     */
    @Override
    public void cleanErrors() {
        mErrorImage.setVisibility(View.GONE);
        mTilName.setError(null);
        mTilDepartment.setError(null);
        mTilCity.setError(null);
        mTilAddress.setError(null);
        mErrorDate.setVisibility(View.GONE);
        mTilEmail.setError(null);
    }

    /**
     * Metodo invocado a partir del resultado de las actividades hijas
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Acciones realizadas al recibir el codigo CANCELED
        if(resultCode == RESULT_CANCELED){

        } else
            //Acciones realizadas al recobir el codigo OK
            if (resultCode == RESULT_OK){

                switch (requestCode){

                    //Procesar la accion despues de tomar la foto de la camara
                    case ACTION_TAKE_PHOTO:
                        mRoutePhoto = Util.updateImage(getBaseContext(), getContentResolver(), mImagePhoto, mImageUri);
                        break;

                    //Procesar la accion despues de seleccionar una foto de la galeria
                    case ACTION_SELECT_PHOTO:
                        mRoutePhoto = Util.updateSelectImage(data, getContentResolver(), mImagePhoto);
                        break;
                }
            }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mImageUri != null) {
            outState.putString("cameraImageUri", mImageUri.toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("cameraImageUri")) {
            mImageUri = Uri.parse(savedInstanceState.getString("cameraImageUri"));
        }
    }


    /**
     * Acciones realizadas al momento de dar clic en el boton atrás
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        endActivity(RESULT_CANCELED);
    }

    /**
     * Retorna el resultado de las acciones realizadas en la actividad
     *
     * @param result
     */
    private void endActivity(int result) {
        Intent intent = getIntent();
        setResult(result, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
