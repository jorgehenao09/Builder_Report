package com.alphatec.builderreport.activities.myProjects;

import com.alphatec.builderreport.activities.myProjects.interfaces.MyProjectsModel;
import com.alphatec.builderreport.persistence.core.BuilderReportDataBase;
import com.alphatec.builderreport.persistence.entities.ProjectE;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Jorge Henao on 13/02/2018.
 */

public class MyProjectsModelImpl implements MyProjectsModel{

    private BuilderReportDataBase mDataBase;

    public MyProjectsModelImpl(BuilderReportDataBase dataBase) {
        this.mDataBase = dataBase;
    }

    /**
     * Retorna la lista de proyectos obtenidos en la base de datos
     *
     * @return
     */
    @Override
    public Flowable<List<ProjectE>> getProjectsDB() {
        return mDataBase.getProjectDao().getAllProjects();
    }
}
