package com.alphatec.builderreport.activities.myProjects.interfaces;

import com.alphatec.builderreport.persistence.entities.ProjectE;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Jorge Henao on 13/02/2018.
 */

public interface MyProjectsModel {

    /**
     * Retorna la lista de proyectos obtenidos en la base de datos
     *
     * @return
     */
    Flowable<List<ProjectE>> getProjectsDB();
}
