package com.alphatec.builderreport.activities.newReport;

import com.alphatec.builderreport.activities.newReport.interfaces.ReportModel;
import com.alphatec.builderreport.activities.newReport.interfaces.ReportView;

/**
 * Created by Jorge Henao on 23/01/2018.
 */

public class ReportPresenter {

    private ReportView mView;
    private ReportModel mModel;

    public ReportPresenter(ReportView view, ReportModel model) {
        this.mView = view;
        this.mModel = model;
    }
}
