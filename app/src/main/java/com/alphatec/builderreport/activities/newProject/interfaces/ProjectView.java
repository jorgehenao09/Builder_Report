package com.alphatec.builderreport.activities.newProject.interfaces;

import com.alphatec.builderreport.core.BaseView;
import com.alphatec.builderreport.persistence.entities.ProjectE;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

public interface ProjectView extends BaseView {

    /**
     * Retorna el POJO de proyecto
     *
     * @return
     */
    ProjectE getProject();

    /**
     * Metodo utilizado para llenar el formulario
     *
     * @param project
     */
    void initializeFormWithData(ProjectE project);

    /**
     * Regresa a la actividad donde fue invocada con codigo OK
     */
    void returnResultActivity();

    /**
     * Realiza las acciones necesarias para notificar el exito en la operacion de guardar un proyecto
     */
    void showSuccessfulSavingMessage();

    /**
     * Metodo utilizado para mostrar errores asociados con la fotografia
     *
     * @param messageCode
     * @param messageParameter
     */
    void showErrorMessagePhoto(String messageCode, Object... messageParameter);

    /**
     * Metodo utilizado para mostrar errores asociados con el nombre
     *
     * @param messageCode
     * @param messageParameter
     */
    void showErrorMessageName(String messageCode, Object... messageParameter);

    /**
     * Metodo utilizado para mostrar errores asociados con el departamento
     *
     * @param messageCode
     * @param messageParameter
     */
    void showErrorMessageDepartment(String messageCode, Object... messageParameter);

    /**
     * Metodo utilizado para mostrar errores asociados con la ciudad
     *
     * @param messageCode
     * @param messageParameter
     */
    void showErrorMessageCity(String messageCode, Object... messageParameter);

    /**
     * Metodo utilizado para mostrar errores asociados con la dirección
     *
     * @param messageCode
     * @param messageParameter
     */
    void showErrorMessageAddress(String messageCode, Object... messageParameter);

    /**
     * Metodo utilizado para mostrar errores asociados con la fecha
     *
     * @param messageCode
     * @param messageParameter
     */
    void showErrorMessageDate(String messageCode, Object... messageParameter);

    /**
     * Metodo utilizado para mostrar errores asociados con el correo electrónico
     *
     * @param messageCode
     * @param messageParameter
     */
    void showErrorMessageEmail(String messageCode, Object... messageParameter);
}
