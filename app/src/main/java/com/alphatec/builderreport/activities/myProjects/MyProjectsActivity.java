package com.alphatec.builderreport.activities.myProjects;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.alphatec.builderreport.R;
import com.alphatec.builderreport.activities.myProjects.interfaces.MyProjectsModel;
import com.alphatec.builderreport.activities.myProjects.interfaces.MyProjectsView;
import com.alphatec.builderreport.activities.newProject.ProjectActivity;
import com.alphatec.builderreport.adapters.RecyclerProjectAdapter;
import com.alphatec.builderreport.core.BaseActivity;
import com.alphatec.builderreport.persistence.core.BuilderReportDataBase;
import com.alphatec.builderreport.persistence.entities.ProjectE;

import java.util.List;

import butterknife.BindView;

public class MyProjectsActivity extends BaseActivity implements MyProjectsView{

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.my_projects_content_empty_data) LinearLayout mLinearContent;
    @BindView(R.id.my_projects_recycler) RecyclerView mRecyclerProjects;
    @BindView(R.id.my_projects_fb_add) FloatingActionButton mAddProject;

    private static final int ACTION_PROJECT = 0;

    private RecyclerProjectAdapter mProjectAdapter;
    private MyProjectsPresenter mPresenter;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildPresenter() {
        super.buildPresenter();

        MyProjectsModel model = new MyProjectsModelImpl(BuilderReportDataBase.getInstance(this));

        mPresenter = new MyProjectsPresenter(this, model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {
        super.initializeView();

        //Se obtiene el listado de proyectos desde la base de datos
        mPresenter.getProjectListDB();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_projects;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getParentContainerId() {
        return R.id.activity_my_projects;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeToolbar() {
        super.initializeToolbar();

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerOnScrollListener();
    }

    /**
     * Acciones realizadas al momento de dar clic sobre el boton agregar
     *
     * @param view
     */
    public void showNewProject(View view){
        Intent intent = new Intent(MyProjectsActivity.this, ProjectActivity.class);
        startActivityForResult(intent, ACTION_PROJECT);
    }

    /**
     * Metodo que determina si se muestra o no el FAB
     */
    private void recyclerOnScrollListener() {
        mRecyclerProjects.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && mAddProject.getVisibility() == View.VISIBLE) {
                    mAddProject.hide();
                } else if (dy < 0 && mAddProject.getVisibility() != View.VISIBLE) {
                    mAddProject.show();
                }
            }
        });
    }

    /**
     * Metodo utilizado para llenar el recycler de proyectos
     *
     * @param applicationList
     */
    @Override
    public void setRecyclerProjects(List<ProjectE> applicationList) {
        //Se oculta el mensaje que indica que no hay datos por mostrar y se muestra el recycler
        mLinearContent.setVisibility(View.GONE);
        mRecyclerProjects.setVisibility(View.VISIBLE);

        //atributo que permite redimensionar el tamaño de las tarjetas al momento de que el usuario cambie la posicion del dispositivo
        mRecyclerProjects.setHasFixedSize(true);
        mRecyclerProjects.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        mProjectAdapter = new RecyclerProjectAdapter(applicationList, this, getApplicationContext());

        mRecyclerProjects.setAdapter(mProjectAdapter);
    }

    @Override
    public void showIndicatorEmptyData() {
        //Se muestra el mensaje que indica que no hay datos por mostrar y se oculta el recycler
        mLinearContent.setVisibility(View.VISIBLE);
        mRecyclerProjects.setVisibility(View.GONE);
    }

    /**
     * Metodo utilizado para limpiar los datos del formulario
     */
    @Override
    public void initializeForm() {
        //Este metodo no se usa en esta actividad
    }

    /**
     * Metodo encargado de limpiar los mensajes de error mostrados en pantalla
     */
    @Override
    public void cleanErrors() {
        //Este metodo no se usa en esta actividad
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
