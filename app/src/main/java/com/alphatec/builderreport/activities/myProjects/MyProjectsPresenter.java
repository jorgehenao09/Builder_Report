package com.alphatec.builderreport.activities.myProjects;

import android.arch.lifecycle.ViewModel;

import com.alphatec.builderreport.activities.myProjects.interfaces.MyProjectsModel;
import com.alphatec.builderreport.activities.myProjects.interfaces.MyProjectsView;
import com.alphatec.builderreport.persistence.entities.ProjectE;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Jorge Henao on 13/02/2018.
 */

public class MyProjectsPresenter extends ViewModel{

    private MyProjectsView mView;
    private MyProjectsModel mModel;

    private CompositeDisposable mDisposeBag;

    public MyProjectsPresenter(MyProjectsView view, MyProjectsModel model) {
        this.mView = view;
        this.mModel = model;

        mDisposeBag = new CompositeDisposable();
    }

    public void getProjectListDB() {
        //Se muestra un mensaje de espera mientras el proceso se realiza
        mView.showWaitMessage();

        //Se obtiene el listado de solicitudes que esten en la Base de datos
        Disposable disposable = mModel.getProjectsDB()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<ProjectE>>() {
                    @Override
                    public void accept(List<ProjectE> projects) throws Exception {
                        //Se llenan las listas de proyectos
                        mView.setRecyclerProjects(projects);
                    }
                }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {

                //Se muestra el indicador de que no hay proyectos para mostrar
                mView.showIndicatorEmptyData();
            }
        });

        mDisposeBag.add(disposable);

        //Esconde el mensaje de espera
        mView.hideWaitMessage();

    }

    @Override
    public void onCleared(){
        //prevents memory leaks by disposing pending observable objects
        if (mDisposeBag != null && !mDisposeBag.isDisposed()) {
            mDisposeBag.clear();
        }
    }

    /**
     * Libera las instancias del view y del model
     */
    public void onDestroy() {
        this.mView = null;
        this.mModel = null;
    }
}
