package com.alphatec.builderreport.activities.myProjects.interfaces;

import com.alphatec.builderreport.core.BaseView;
import com.alphatec.builderreport.persistence.entities.ProjectE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jorge Henao on 13/02/2018.
 */

public interface MyProjectsView extends BaseView{

    /**
     * Metodo utilizado para llenar el recycler de proyectos
     *
     * @param applicationList
     */
    void setRecyclerProjects(List<ProjectE> applicationList);

    /**
     * Esconde el recycler y muestra el indicador de que no hay proyectos para mostrar
     */
    void showIndicatorEmptyData();
}
