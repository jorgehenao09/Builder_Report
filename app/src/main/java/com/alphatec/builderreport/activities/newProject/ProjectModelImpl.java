package com.alphatec.builderreport.activities.newProject;

import com.alphatec.builderreport.activities.newProject.interfaces.ProjectModel;
import com.alphatec.builderreport.util.ResponseProcess;
import com.alphatec.builderreport.constants.Constants;
import com.alphatec.builderreport.persistence.core.BuilderReportDataBase;
import com.alphatec.builderreport.persistence.entities.ProjectE;
import com.alphatec.builderreport.util.Util;

import java.util.Date;

/**
 * Created by Jorge Henao on 11/02/2018.
 */

public class ProjectModelImpl implements ProjectModel {

    private BuilderReportDataBase mDataBase;

    public ProjectModelImpl(BuilderReportDataBase dataBase) {
        this.mDataBase = dataBase;
    }

    /**
     * Define los paso al guardar un proyecto
     *
     * @param project
     * @return
     */
    @Override
    public ResponseProcess saveProject(final ProjectE project) {
        ResponseProcess response = new ResponseProcess();

        //Se verifica que el campo fotografia no este vacio
        if (Util.itsEmptyData(project.getRoutePhoto())){
            response.addErrorMessage(Constants.PHOTO, Constants.REQUIRED_VALUE);
        }

        //Se verifica que el campo nombre no este vacio y este entre 1 y 30 caracteres
        if (Util.itsEmptyData(project.getNameProject())){
            response.addErrorMessage(Constants.NAME, Constants.REQUIRED_VALUE);
        }else if(project.getNameProject().toString().length() > 30){
            response.addErrorMessage(Constants.NAME, Constants.SIZE_OUT_RANGE, 1, 30);
        }

        //Se verifica que el campo departamento no este vacio y este entre 1 y 30 caracteres
        if (Util.itsEmptyData(project.getDempartment())){
            response.addErrorMessage(Constants.DEPARTMENT, Constants.REQUIRED_VALUE);
        }else if(project.getNameProject().toString().length() > 30){
            response.addErrorMessage(Constants.DEPARTMENT, Constants.SIZE_OUT_RANGE, 1, 30);
        }

        //Se verifica que el campo ciudad no este vacio y este entre 1 y 30 caracteres
        if (Util.itsEmptyData(project.getCity())){
            response.addErrorMessage(Constants.CITY, Constants.REQUIRED_VALUE);
        }else if(project.getNameProject().toString().length() > 30){
            response.addErrorMessage(Constants.CITY, Constants.SIZE_OUT_RANGE, 1, 30);
        }

        //Se verifica que el campo dirección no este vacio y este entre 1 y 30 caracteres
        if (Util.itsEmptyData(project.getAddress())){
            response.addErrorMessage(Constants.ADDRESS, Constants.REQUIRED_VALUE);
        }else if(project.getNameProject().toString().length() > 30){
            response.addErrorMessage(Constants.ADDRESS, Constants.SIZE_OUT_RANGE, 1, 30);
        }

        //Se verifica que el campo fecha de siembra no este vacio y sea menor a la fecha actual
        if(project.getDate() == null){
            response.addErrorMessage(Constants.DATE, Constants.REQUIRED_VALUE);
        }else if (project.getDate().compareTo(Util.deleteHourDate(new Date())) > 0 ){
            response.addErrorMessage(Constants.DATE, Constants.DATE_GREATER_THAN_CURRENT);
        }

        //Se verifica que el campo fotografia no este vacio
        if (Util.itsEmptyData(project.getEmail())){
            response.addErrorMessage(Constants.EMAIL, Constants.REQUIRED_VALUE);
        }else if (!Util.isEmailValid(project.getEmail())){
            response.addErrorMessage(Constants.EMAIL, Constants.INAVLID_EMAIL);
        }

        //Si todas las validaciones son superadas, se procede a guardar el registro
        if(!response.containsErrorMessages()){
            //Si no tiene ID se asume que es un registro nuevo
            if(project.getId() == null) {
                //Se guarda el registro en la BD
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mDataBase.getProjectDao().insert(project);
                    }
                }).start();
            }
            //Si tiene ID, se asume que es un registro existente
            else{
                //Se actualiza el registro en la BD
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mDataBase.getProjectDao().update(project);
                    }
                }).start();
            }
        }

        return response;
    }
}
